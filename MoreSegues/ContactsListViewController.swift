//
//  ContactsListViewController.swift
//  MoreSegues
//
//  Created by mac on 5/10/19.
//  Copyright © 2019 com. All rights reserved.
//

import UIKit

class ContactsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate{
    var contacts: [Contact] = [
        Contact(image: nil, name: "John", phone: "(888) 555 5512"),
        Contact(image: nil, name: "Kate", phone: "(888) 123 5412")
    ]
    
    var myContact: Contact = Contact(image: nil, name: "Irina", phone: "(111) 333 1234")
    private var selectedItem = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.dataSource = self
        tableView?.delegate = self
    }
    
    @IBAction func showMyCard(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "Show My Contact", sender: sender)
    }
    @IBAction func addContact(_ sender: UIBarButtonItem)
    {
        performSegue(withIdentifier: "Add Contact", sender: sender)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath)
        let contact = contacts[indexPath.row]
        cell.textLabel?.text = contact.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    @IBOutlet weak var tableView: UITableView?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if let currentCell = tableView.cellForRow(at: indexPath) as?  UITableViewCell{
            selectedItem = indexPath.row
            performSegue(withIdentifier: "Details Segue", sender: currentCell)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Details Segue"{
            if let destination = segue.destination as? DetailContactViewController{
                destination.contact = contacts[selectedItem]
            }
        } else if segue.identifier == "Show My Contact"{
            if let destination = segue.destination as? DetailContactViewController{
                destination.contact = myContact
                if let ppc = destination.popoverPresentationController{
                    ppc.delegate = self
                }
            }
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    @IBAction func goBack(_ sender: UIStoryboardSegue){
        tableView?.reloadData()
    }
    
}
